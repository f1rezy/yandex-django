from django.core.exceptions import ValidationError
from django.db import models

from catalog.models import Item
from rating.managers import RatingManager
from users.models import User


__all__ = []


STATUS_CHOICES = [
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5),
]
# STATUS_CHOICES_DICT = {
#     "Ненависть": 1,
#     "Неприязнь": 2,
#     "Нейтрально": 3,
#     "Обожание": 4,
#     "Любовь": 5,
# }


class Rating(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name="пользователь",
        related_name="ratings",
    )
    item = models.ForeignKey(
        Item,
        on_delete=models.CASCADE,
        verbose_name="товар",
        related_name="ratings",
    )
    rating = models.IntegerField(
        "оценка",
        choices=STATUS_CHOICES,
        default=3,
    )

    objects = RatingManager()

    class Meta:
        verbose_name = "оценка"
        verbose_name_plural = "оценки"
        default_related_name = "ratings"

    def __str__(self):
        return self.rating

    def save(self, *args, **kwargs):
        if (
            type(self)
            .objects.filter(
                user_id=self.user.id,
                item_id=self.item.id,
            )
            .exclude(id=self.id)
            .count()
            > 0
        ):
            raise ValidationError(
                "Один пользователь может поставить только одну оценку товару",
            )
        super().save(*args, **kwargs)

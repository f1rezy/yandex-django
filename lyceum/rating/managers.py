import django.db.models

import catalog.models
import rating.models
import users.models

__all__ = []


class RatingManager(django.db.models.Manager):
    def by_users(self):
        statistics_by_users = {}
        active_users_with_ratings = users.models.User.objects.active()
        for user in active_users_with_ratings:
            max_rated_item_data = (
                user.ratings.all()
                .order_by(
                    f"-{rating.models.Rating.rating.field.name}",
                )
                .values(
                    rating.models.Rating.rating.field.name,
                    rating.models.Rating.item.field.name,
                )
                .first()
            )
            min_rated_item_data = (
                user.ratings.all()
                .order_by(
                    f"{rating.models.Rating.rating.field.name}",
                )
                .values(
                    rating.models.Rating.rating.field.name,
                    rating.models.Rating.item.field.name,
                )
                .first()
            )
            statistics_by_users[user] = {
                "max": max_rated_item_data,
                "min": min_rated_item_data,
                "average": user.ratings.all().aggregate(
                    django.db.models.Avg(
                        rating.models.Rating.rating.field.name,
                    ),
                )["rating__avg"],
                "amount": len(user.ratings.all()),
            }
        return statistics_by_users

    def by_items(self):
        statistics_by_items = {}
        active_items_with_ratings = catalog.models.Item.objects.published()
        for item in active_items_with_ratings:
            last_max_rating = (
                item.ratings.all()
                .order_by(
                    f"-{rating.models.Rating.rating.field.name}",
                )
                .values(
                    rating.models.Rating.rating.field.name,
                    rating.models.Rating.user.field.name,
                )
                .first()
            )
            last_min_rating = (
                item.ratings.all()
                .order_by(
                    f"{rating.models.Rating.rating.field.name}",
                )
                .values(
                    rating.models.Rating.rating.field.name,
                    rating.models.Rating.user.field.name,
                )
                .first()
            )
            statistics_by_items[item] = {
                "max": last_max_rating,
                "min": last_min_rating,
                "average": item.ratings.all().aggregate(
                    django.db.models.Avg(
                        rating.models.Rating.rating.field.name,
                    ),
                )["rating__avg"],
                "amount": len(item.ratings.all()),
            }
        return statistics_by_items

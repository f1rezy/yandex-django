# Generated by Django 4.2.5 on 2023-11-25 12:13

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("rating", "0002_rating_score"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="rating",
            name="score",
        ),
        migrations.AlterField(
            model_name="rating",
            name="rating",
            field=models.PositiveIntegerField(
                choices=[
                    ("Ненависть", 1),
                    ("Неприязнь", 2),
                    ("Нейтрально", 3),
                    ("Обожание", 4),
                    ("Любовь", 5),
                ],
                default=3,
                verbose_name="оценка",
            ),
        ),
    ]

from django.contrib import admin

from catalog.models import Category, Image, Item, MainImage, Tag

__all__ = []

admin.site.register(Category)
admin.site.register(Tag)


class MainImage(admin.TabularInline):
    model = MainImage
    fields = ("image",)


class Image(admin.TabularInline):
    model = Image
    fields = ("image",)


class ItemInline(admin.TabularInline):
    model = Item


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = (
        Item.name.field.name,
        Item.is_published.field.name,
        Item.image_tmb,
    )
    list_editable = (Item.is_published.field.name,)
    list_display_links = (Item.name.field.name,)
    filter_horizontal = (Item.tags.field.name,)
    readonly_fields = (
        Item.updated.field.name,
        Item.created.field.name,
    )
    inlines = (
        MainImage,
        Image,
    )

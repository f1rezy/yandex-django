from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.safestring import mark_safe
from django_ckeditor_5.fields import CKEditor5Field

from catalog.validators import WordsValidator
from core.models import Base, ImageBase

__all__ = []


class Category(Base):
    slug = models.SlugField(
        "слаг",
        max_length=200,
        help_text="Максимум 200 символов",
        unique=True,
    )

    weight = models.PositiveSmallIntegerField(
        "вес",
        validators=[
            MinValueValidator(
                1,
                message="Значение должно быть больше 0",
            ),
            MaxValueValidator(
                32767,
                message="Значение должно быть меньше 32767",
            ),
        ],
        default=100,
        help_text="Max 32767",
    )

    class Meta:
        ordering = ("name", "id")
        verbose_name = "категория"
        verbose_name_plural = "категории"

    def __str__(self):
        return self.name


class Tag(Base):
    slug = models.SlugField(
        "слаг",
        max_length=200,
        help_text="Максимум 200 символов",
        unique=True,
    )

    class Meta:
        ordering = ("slug", "id")
        verbose_name = "тег"
        verbose_name_plural = "теги"
        default_related_name = "tags"

    def __str__(self):
        return self.name


class ItemManager(models.Manager):
    def on_main(self):
        return (
            self.published()
            .filter(
                is_on_main=True,
            )
            .order_by(
                Item.name.field.name,
            )
        )

    def published(self):
        return (
            self.get_queryset()
            .filter(
                is_published=True,
                category__is_published=True,
            )
            .order_by(
                f"{Item.category.field.name}__{Category.name.field.name}",
                Item.name.field.name,
            )
            .select_related(
                Item.category.field.name,
                Item.main_image.related.name,
            )
            .prefetch_related(
                models.Prefetch(
                    Item.tags.field.name,
                    queryset=Tag.objects.filter(is_published=True).only(
                        Tag.name.field.name,
                    ),
                ),
            )
            .only(
                Item.name.field.name,
                Item.text.field.name,
                Item.main_image.related.name,
                f"{Item.category.field.name}__{Category.name.field.name}",
                f"{Item.tags.field.name}__{Tag.name.field.name}",
            )
        )


class Item(Base):
    objects = ItemManager()

    category = models.ForeignKey(
        Category,
        on_delete=models.CASCADE,
        verbose_name="категория",
        help_text="Выберите категорию",
    )
    is_on_main = models.BooleanField(
        "на главной",
        default=False,
    )
    tags = models.ManyToManyField(Tag)
    text = CKEditor5Field(
        "текст",
        validators=[
            WordsValidator(
                "превосходно",
                "роскошно",
            ),
        ],
        help_text=(
            "Описание должно содержать слова 'превосходно' или 'роскошно'"
        ),
    )
    updated = models.DateTimeField(
        "время изменения",
        auto_now=True,
        null=True,
        blank=True,
    )
    created = models.DateTimeField(
        "время создания",
        auto_now_add=True,
        null=True,
        blank=True,
    )

    class Meta:
        ordering = ("name", "id")
        verbose_name = "товар"
        verbose_name_plural = "товары"
        default_related_name = "items"

    def __str__(self):
        return self.text[:15]

    def image_tmb(self):
        if self.main_image.image:
            return mark_safe(
                f"<img src='{self.main_image.get_image_50x50.url}'>",
            )
        return "Нет изображения"

    image_tmb.short_description = "превью"
    image_tmb.allow_tags = True


class MainImage(ImageBase):
    item = models.OneToOneField(
        Item,
        on_delete=models.CASCADE,
        related_name="main_image",
    )

    def __str__(self):
        return self.item.name

    class Meta:
        verbose_name = "главное изображение"
        verbose_name_plural = "главные изображения"


class Image(ImageBase):
    item = models.ForeignKey(
        Item,
        on_delete=models.CASCADE,
        related_name="images",
    )

    class Meta:
        verbose_name = "фото"
        verbose_name_plural = "фото"

from django.urls import path

from catalog import views


app_name = "catalog"

urlpatterns = [
    path("", views.ItemListView.as_view(), name="item_list"),
    path("<int:pk>/", views.ItemDetailView.as_view(), name="item_detail"),
    path("new/", views.ItemNewView.as_view(), name="new"),
    path("friday/", views.ItemFridayView.as_view(), name="friday"),
    path("unverified/", views.ItemUnverifiedView.as_view(), name="unverified"),
]

from django.core.exceptions import ValidationError
from django.test import Client, TestCase
from django.urls import reverse
from parameterized import parameterized

from catalog.models import Category, Item, Tag

__all__ = []


class StaticURLTests(TestCase):
    fixtures = ["data.json"]

    def test_catalog_endpoint(self):
        response = Client().get("/catalog/")
        self.assertEqual(response.status_code, 200)

    @parameterized.expand(
        [
            ("1", 200),
            ("4", 200),
            ("100", 404),
            ("0", 404),
            ("-0", 404),
            ("-100", 404),
            ("0.5", 404),
            ("abc", 404),
            ("0abc", 404),
            ("0abc", 404),
            ("abc0", 404),
            ("$%^", 404),
            ("1e5", 404),
        ],
    )
    def test_catalog_item_endpoint(self, url, expected_status):
        response = Client().get(f"/catalog/{url}/")
        self.assertEqual(response.status_code, expected_status)


class ModelsTests(TestCase):
    def setUp(self):
        self.category = Category.objects.create(
            name="Test category",
            slug="test-category",
        )
        self.tag = Tag.objects.create(
            name="Test tag",
            slug="test-tag",
        )

        super(ModelsTests, self).setUp()

    def tearDown(self):
        Item.objects.all().delete()
        Tag.objects.all().delete()
        Category.objects.all().delete()

        super(ModelsTests, self).tearDown()

    @parameterized.expand(
        [
            ("Превосходно",),
            ("роскошно",),
            ("роскошно!",),
            ("роскошно@",),
            ("!роскошно",),
            ("не роскошно",),
        ],
    )
    def test_item_validator(self, text):
        items_count = Item.objects.count()

        item = Item(
            name="Тестовый товар",
            category=self.category,
            text=text,
        )
        item.full_clean()
        item.save()
        item.tags.add(self.tag)

        self.assertEqual(
            Item.objects.count(),
            items_count + 1,
        )

    @parameterized.expand(
        [
            ("Прев!осходно",),
            ("роскошный",),
            ("роскошное!",),
            ("оскошно@",),
            ("р оскошно",),
            ("qwertyроскошно",),
        ],
    )
    def test_item_negative_validator(self, text):
        items_count = Item.objects.count()

        with self.assertRaises(ValidationError):
            item = Item(
                name="Тестовый товар",
                category=self.category,
                text=text,
            )
            item.full_clean()
            item.save()
            item.tags.add(self.tag)

        self.assertEqual(
            Item.objects.count(),
            items_count,
        )

    @parameterized.expand(
        [
            (1,),
            (100,),
            (32000,),
        ],
    )
    def test_category_validator(self, weight):
        categories_count = Category.objects.count()

        test_category = Category(
            name="Тестовая категория",
            weight=weight,
            slug="test-cat",
        )
        test_category.full_clean()
        test_category.save()

        self.assertEqual(
            Category.objects.count(),
            categories_count + 1,
        )

    @parameterized.expand(
        [
            (-100,),
            (0,),
            (64000,),
        ],
    )
    def test_category_negative_validator(self, weight):
        categories_count = Category.objects.count()

        with self.assertRaises(ValidationError):
            test_category = Category(
                name="Тестовая категория",
                weight=weight,
                slug="test-cat",
            )
            test_category.full_clean()
            test_category.save()

        self.assertEqual(
            Category.objects.count(),
            categories_count,
        )


class CheckFieldsTestCase(TestCase):
    def check_content_value(
        self,
        item,
        exists,
        prefetched,
        not_loaded,
    ):
        check_dict = item.__dict__

        for value in exists:
            self.assertIn(value, check_dict)

        for value in prefetched:
            self.assertIn(value, check_dict["_prefetched_objects_cache"])

        for value in not_loaded:
            self.assertNotIn(value, check_dict)


class CatalogItemsTests(CheckFieldsTestCase):
    fixtures = ["data.json"]

    def test_items_in_context(self):
        response = Client().get(reverse("catalog:item_list"))
        self.assertIn("items", response.context)

    def test_items_size(self):
        response = Client().get(reverse("catalog:item_list"))
        self.assertEqual(len(response.context["items"]), 2)

    def test_items_types(self):
        response = Client().get(reverse("catalog:item_list"))
        self.assertTrue(
            all(
                isinstance(
                    item,
                    Item,
                )
                for item in response.context["items"]
            ),
        )

    def test_items_loaded_values(self):
        response = Client().get(reverse("catalog:item_list"))
        for item in response.context["items"]:
            self.check_content_value(
                item,
                (
                    "name",
                    "text",
                    "category_id",
                ),
                ("tags",),
                (
                    "is_on_main",
                    "image",
                    "images",
                    "is_published",
                ),
            )


class DetailItemTests(CheckFieldsTestCase):
    fixtures = ["data.json"]

    def test_item_in_context(self):
        response = Client().get(reverse("catalog:item_detail", args=[1]))
        self.assertIn("item", response.context)

    def test_item_type(self):
        response = Client().get(reverse("catalog:item_detail", args=[1]))
        self.assertIsInstance(
            response.context["item"],
            Item,
        )

    def test_item_loaded_values(self):
        response = Client().get(reverse("catalog:item_detail", args=[1]))
        self.check_content_value(
            response.context["item"],
            (
                "name",
                "text",
                "category_id",
            ),
            ("tags",),
            (
                "is_on_main",
                "image",
                "is_published",
            ),
        )
        self.check_content_value(
            response.context["item"].tags.all()[0],
            ("name",),
            (),
            ("is_published",),
        )

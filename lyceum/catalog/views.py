import datetime
from random import sample

from django.contrib import messages
from django.db import models
from django.shortcuts import redirect
from django.views import generic

from catalog.models import Image, Item
from rating.forms import RatingForm
from rating.models import Rating

__all__ = []

ITEMS_PER_PAGE = 5


class ItemNewView(generic.DetailView):
    template_name = "catalog/special.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        items_ids = list(
            Item.objects.published()
            .filter(
                created__gte=datetime.date.today()
                - datetime.timedelta(days=7),
            )
            .values_list("id", flat=True),
        )

        try:
            selected = sample(items_ids, ITEMS_PER_PAGE)
        except ValueError:
            selected = items_ids

        context["items"] = Item.objects.published().filter(pk__in=selected)
        context["title"] = "Новинки"
        return context


class ItemFridayView(generic.DetailView):
    template_name = "catalog/special.html"
    context_object_name = "items"
    queryset = (
        Item.objects.published()
        .filter(updated__week_day=6)
        .order_by(
            f"-{Item.updated.field.name}",
        )[:5]
    )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Пятница"
        return context


class ItemUnverifiedView(generic.DetailView):
    template_name = "catalog/special.html"
    context_object_name = "items"
    queryset = (
        Item.objects.published()
        .filter(
            created__gte=models.F(
                Item.updated.field.name,
            )
            - datetime.timedelta(seconds=1),
            created__lte=models.F(
                Item.updated.field.name,
            )
            + datetime.timedelta(seconds=1),
        )
        .order_by(
            "?",
        )[:5]
    )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Непроверенное"
        return context


class ItemListView(generic.ListView):
    template_name = "catalog/item_list.html"
    context_object_name = "items"
    queryset = Item.objects.published()


class ItemDetailView(generic.DetailView):
    template_name = "catalog/item_detail.html"
    context_object_name = "item"
    queryset = Item.objects.published().prefetch_related(
        models.Prefetch(
            Item.images.field.related_query_name(),
            queryset=Image.objects.only(
                Image.image.field.name,
                Image.item_id.field.name,
            ),
        ),
    )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        item_id = kwargs.get("object").id

        if self.request.user.is_authenticated:
            try:
                current_user_rating = Rating.objects.get(
                    user_id=self.request.user.id,
                    item_id=item_id,
                )
                rating_form = RatingForm(instance=current_user_rating)
            except Rating.DoesNotExist:
                rating_form = RatingForm()
            context["rating_form"] = rating_form

        context["ratings"] = Rating.objects.filter(item_id=item_id)
        context["ratings_count"] = len(context["ratings"])
        context["average_rating"] = (
            sum(
                map(lambda x: x.rating or 0, context["ratings"]),
            )
            / context["ratings_count"]
            if context["ratings_count"] != 0
            else 0
        )
        return context

    def post(self, request, *args, **kwargs):
        rating_form = RatingForm(request.POST)
        if rating_form.is_valid():
            try:
                rating = Rating.objects.get(
                    user=request.user,
                    item_id=kwargs.get("pk"),
                )
            except Rating.DoesNotExist:
                rating = Rating(
                    user=request.user,
                    item_id=kwargs.get("pk"),
                )
            rating.rating = rating_form.cleaned_data.get("rating")
            rating.save()
            messages.success(request, "Данные успешно сохранены!")
        return redirect("catalog:item_detail", kwargs.get("pk"))

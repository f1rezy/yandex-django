from http import HTTPStatus

from django.test import Client, TestCase
from django.urls import reverse

from catalog.models import Category, Item, Tag

__all__ = []


class StaticURLTests(TestCase):
    def test_homepage_endpoint(self):
        response = Client().get(reverse("homepage:home"))
        self.assertEqual(response.status_code, 200)

    def test_coffee_endpoint_status(self):
        response = Client().get(reverse("homepage:coffee"))
        self.assertEqual(response.status_code, HTTPStatus.IM_A_TEAPOT)

    def test_coffee_endpoint_text(self):
        response = Client().get(reverse("homepage:coffee"))
        self.assertEqual(response.content, "Я чайник".encode())


class ContextTests(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.published_category = Category.objects.create(
            is_published=True,
            name="Тестовая опубликованная категория",
            slug="published_category",
            weight=100,
        )
        cls.unpublished_category = Category.objects.create(
            is_published=False,
            name="Тестовая неопубликованная категория",
            slug="unpublished_category",
            weight=100,
        )
        cls.published_tag = Tag.objects.create(
            is_published=True,
            name="Тестовый опубликованный тэг",
            slug="published_tag",
        )
        cls.unpublished_tag = Tag.objects.create(
            is_published=False,
            name="Тестовый неопубликованный тэг",
            slug="unpublished_tag",
        )
        cls.on_main_item = Item.objects.create(
            is_on_main=True,
            is_published=True,
            name="Тестовый товар на главной",
            category=cls.published_category,
            text="превосходно",
        )
        cls.not_on_main_item = Item.objects.create(
            is_on_main=False,
            is_published=True,
            name="Тестовый товар не на главной",
            category=cls.published_category,
            text="превосходно",
        )

        cls.published_category.save()
        cls.unpublished_category.save()

        cls.published_tag.save()
        cls.unpublished_tag.save()

        cls.on_main_item.clean()
        cls.on_main_item.save()
        cls.not_on_main_item.clean()
        cls.not_on_main_item.save()

        cls.on_main_item.tags.add(cls.published_tag.pk)
        cls.on_main_item.tags.add(cls.unpublished_tag.pk)

    def test_home_page_show_correct_context(self):
        response = Client().get(
            reverse("homepage:home"),
        )
        self.assertIn("items", response.context)

    def test_nome_count_item(self):
        response = Client().get(
            reverse("homepage:home"),
        )
        items = response.context["items"]
        self.assertEqual(len(items), 1)


class HomepageItemsTests(TestCase):
    fixtures = ["data.json"]

    def test_items_in_context(self):
        response = Client().get(reverse("homepage:home"))
        self.assertIn("items", response.context)

    def test_items_size(self):
        response = Client().get(reverse("homepage:home"))
        self.assertEqual(len(response.context["items"]), 2)

    def test_items_types(self):
        response = Client().get(reverse("homepage:home"))
        self.assertTrue(
            all(
                isinstance(
                    item,
                    Item,
                )
                for item in response.context["items"]
            ),
        )

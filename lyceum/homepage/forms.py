from django import forms

__all__ = []


class EchoForm(forms.Form):
    text = forms.CharField(
        widget=forms.TextInput(attrs={"class": "form-control"}),
    )

from http import HTTPStatus

from django.contrib.auth.models import User
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.views import generic, View

from catalog.models import Item
from homepage.forms import EchoForm


__all__ = []


class HomeView(generic.ListView):
    template_name = "homepage/home.html"
    context_object_name = "items"
    queryset = Item.objects.on_main()


class CoffeeView(View):
    def get(self, request):
        if request.user.is_authenticated:
            user = User.objects.get(pk=request.user.id)
            user.profile.coffee_count += 1
            user.profile.save()
        return HttpResponse("Я чайник", status=HTTPStatus.IM_A_TEAPOT)


class EchoView(generic.FormView):
    form_class = EchoForm
    template_name = "homepage/echo.html"
    success_url = reverse_lazy("homepage:echo_submit")


class EchoSubmitView(View):
    form_class = EchoForm

    def get(self, request, *args, **kwargs):
        return HttpResponse(status=HTTPStatus.METHOD_NOT_ALLOWED)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        text = ""

        if form.is_valid():
            text = form.cleaned_data.get("text")

        return HttpResponse(text)

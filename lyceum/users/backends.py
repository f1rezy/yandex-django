from django.contrib.auth.backends import BaseBackend

from users.models import User

__all__ = []


class UserBackend(BaseBackend):
    def authenticate(self, request, **kwargs):
        email = kwargs["username"]
        password = kwargs["password"]
        try:
            user = User.objects.by_mail(email=email)
            if user.check_password(password):
                return user
        except User.DoesNotExist:
            pass
        return None

    def get_user(self, user_id):
        try:
            user = User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
        return user

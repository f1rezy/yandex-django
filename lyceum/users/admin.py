from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from users.models import Profile

__all__ = []


class ProfileInlined(admin.TabularInline):
    model = Profile
    readonly_fields = (Profile.coffee_count.field.name,)
    can_delete = False


class UserAdmin(BaseUserAdmin):
    inlines = (ProfileInlined,)


admin.site.unregister(User)
admin.site.register(User, UserAdmin)

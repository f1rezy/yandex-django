from sys import argv

from django.contrib.auth.models import BaseUserManager, User
from django.db import models
from sorl.thumbnail import get_thumbnail

__all__ = []


class Profile(models.Model):
    def get_path(self, filename):
        return f"users/{self.user_id}/{filename}"

    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        related_name="profile",
    )
    birthday = models.DateField(
        "дата рождения",
        blank=True,
        null=True,
    )
    image = models.ImageField(
        "аватарка",
        upload_to=get_path,
        blank=True,
    )
    coffee_count = models.PositiveIntegerField(
        "количество переходов по /coffee/",
        default=0,
    )

    def get_image_300x300(self):
        return get_thumbnail(self.image, "300x300", crop="center", quality=51)

    class Meta:
        verbose_name = "Дополнительное поле"
        verbose_name = "Дополнительные поля"


class UserManager(BaseUserManager):
    def active(self):
        return (
            self.get_queryset()
            .filter(
                is_active=True,
            )
            .select_related(
                User.profile.related.name,
            )
        )

    def by_mail(self, email):
        return self.active().get(
            email=email,
        )


class User(User):
    objects = UserManager()

    class Meta:
        proxy = True


if "makemigrations" not in argv and "migrate" not in argv:
    User._meta.get_field("email")._unique = True

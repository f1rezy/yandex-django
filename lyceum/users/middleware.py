from users.models import User

__all__ = []


class UserMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        try:
            request.user = User.objects.get(pk=request.user.id)
        except User.DoesNotExist:
            pass
        return self.get_response(request)

from datetime import timedelta
from http import HTTPStatus

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.mail import send_mail
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse_lazy
from django.utils import timezone
from django.views import generic, View

from users.forms import ProfileForm, SignupForm, UserForm
from users.models import Profile, User


__all__ = []


class SignupView(generic.FormView):
    model = User
    form_class = SignupForm
    template_name = "users/signup.html"
    success_url = reverse_lazy("users:signup")

    def form_valid(self, form):
        username = form.cleaned_data.get("username")
        new_user = form.save(commit=False)
        new_user.set_password(form.cleaned_data["password1"])
        new_user.is_active = settings.DEFAULT_USER_IS_ACTIVE
        new_user.save()
        send_mail(
            "Subject",
            "Для активации пользователя перейдите на эту страницу:\n"
            f"http://127.0.0.1:8000/auth/activate/{username}/",
            settings.MAIL,
            [username],
            fail_silently=False,
        )

        messages.success(self.request, "Форма успешно отправлена!")
        return super().form_valid(form)


class ActivateView(View):
    def get(self, request, *args, **kwargs):
        user = get_object_or_404(User, username=kwargs.get("username"))
        enddate = user.date_joined + timedelta(hours=12)

        if timezone.now() > enddate:
            return HttpResponse(status=HTTPStatus.METHOD_NOT_ALLOWED)

        user.is_active = True
        user.save()

        return render(request, "users/activate_done.html", {})


class UserListView(generic.ListView):
    template_name = "users/user_list.html"
    context_object_name = "users"
    queryset = (
        User.objects.filter(
            is_active=True,
        )
        .order_by(
            User.username.field.name,
        )
        .only(
            User.username.field.name,
        )
    )


class UserDetailView(generic.DetailView):
    template_name = "users/user_detail.html"
    context_object_name = "user"
    queryset = (
        User.objects.filter(
            is_active=True,
        )
        .select_related(
            User.profile.related.name,
        )
        .only(
            User.email.field.name,
            User.first_name.field.name,
            User.last_name.field.name,
            f"{User.profile.related.name}__{Profile.birthday.field.name}",
            f"{User.profile.related.name}__{Profile.image.field.name}",
            f"{User.profile.related.name}__{Profile.coffee_count.field.name}",
        )
    )


class ProfileView(LoginRequiredMixin, generic.TemplateView):
    template_name = "users/profile.html"

    def post(self, request, *args, **kwargs):
        user = User.objects.get(pk=request.user.id)
        user_form = UserForm(request.POST or None, instance=user)
        try:
            profile_form = ProfileForm(
                request.POST or None,
                request.FILES or None,
                instance=user.profile,
            )
        except Exception:
            profile_form = ProfileForm(
                request.POST,
                request.FILES,
            )

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, "Данные успешно сохранены!")
            return redirect("users:profile")

        return self.render_to_response(
            {
                "user_form": user_form,
                "profile_form": profile_form,
            },
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = User.objects.get(pk=self.request.user.id)
        user_form = UserForm(self.request.POST or None, instance=user)
        try:
            profile_form = ProfileForm(
                self.request.POST or None,
                instance=user.profile,
            )
        except Exception:
            profile_form = ProfileForm(self.request.POST or None)
        context["user_form"] = user_form
        context["profile_form"] = profile_form
        return context

from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm

from users.models import Profile, User

__all__ = []


class SignupForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = (
            User.username.field.name,
            "password1",
            "password2",
        )


class UserForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User
        fields = (
            User.email.field.name,
            User.first_name.field.name,
            User.last_name.field.name,
        )


class ProfileForm(forms.ModelForm):
    coffee_count = forms.IntegerField(
        label="Количество переходов по /coffee/",
        disabled=True,
    )

    class Meta:
        model = Profile
        fields = (
            Profile.birthday.field.name,
            Profile.image.field.name,
            Profile.coffee_count.field.name,
        )

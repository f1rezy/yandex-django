from django.apps import AppConfig

__all__ = []


class RatingStatisticsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "rating_statistics"
    verbose_name = "Статистика"

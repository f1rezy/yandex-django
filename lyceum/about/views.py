from django.views import generic

__all__ = []


class AboutView(generic.base.TemplateView):
    template_name = "about/about.html"

from django.db import models
from sorl.thumbnail import get_thumbnail

__all__ = []


def item_directory_path(instance, filename):
    return f"catalog/{instance.item.id}/{filename}"


class Base(models.Model):
    is_published = models.BooleanField(
        "опубликовано",
        default=True,
    )
    name = models.CharField(
        "название",
        max_length=150,
        help_text="max 150 символов",
        unique=True,
    )

    class Meta:
        abstract = True


class ImageBase(models.Model):
    image = models.ImageField(
        "изображение",
        upload_to=item_directory_path,
        default=None,
    )

    def get_image_300x300(self):
        return get_thumbnail(self.image, "300x300", crop="center", quality=51)

    @property
    def get_image_50x50(self):
        return get_thumbnail(self.image, "50x50", crop="center", quality=51)

    def __str__(self):
        return self.item.name

    class Meta:
        abstract = True

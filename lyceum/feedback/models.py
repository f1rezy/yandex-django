from django.conf import settings
from django.db import models

__all__ = []

STATUS_CHOICES = [
    ("получено", "получено"),
    ("в обработке", "в обработке"),
    ("ответ дан", "ответ дан"),
]


class Feedback(models.Model):
    name = models.CharField(
        "имя",
        max_length=100,
        null=True,
        blank=True,
    )
    status = models.CharField(
        "статус обработки",
        choices=STATUS_CHOICES,
        default="получено",
        max_length=11,
    )
    text = models.TextField(
        "текстовое поле",
    )
    created_on = models.DateTimeField(
        "дата и время создания",
        auto_now_add=True,
        editable=False,
    )
    mail = models.EmailField(
        "почта",
    )

    class Meta:
        verbose_name = "отзыв"
        verbose_name_plural = "отзывы"
        default_related_name = "feedbacks"

    def __str__(self):
        return self.text[:15]


class StatusLog(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        verbose_name="пользователь",
        related_name="status_logs",
    )
    timestamp = models.TimeField(
        "время",
        auto_now_add=True,
    )
    feedback = models.ForeignKey(
        Feedback,
        on_delete=models.CASCADE,
        verbose_name="отзыв",
        related_name="status_logs",
    )
    from_status = models.CharField(
        "статус обработки до",
        db_column="from",
        max_length=11,
    )
    to = models.CharField(
        "статус обработки после",
        max_length=11,
    )

    class Meta:
        verbose_name = "лог статуса"
        verbose_name_plural = "логи статуса"
        default_related_name = "status_logs"

    def __str__(self):
        return str(self.id)

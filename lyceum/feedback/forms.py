from django import forms

from feedback.models import Feedback

__all__ = []


class FeedbackForm(forms.ModelForm):
    name = forms.CharField(
        max_length=100,
        required=False,
        label=Feedback.name.field.name,
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.visible_fields():
            field.field.widget.attrs["class"] = "form-control"

    class Meta:
        model = Feedback
        exclude = [
            Feedback.created_on.field.name,
            Feedback.status.field.name,
        ]
        labels = {
            Feedback.name.field.name: Feedback.name.field.verbose_name,
            Feedback.text.field.name: Feedback.text.field.verbose_name,
            Feedback.mail.field.name: Feedback.mail.field.verbose_name,
        }
        help_texts = {
            Feedback.name.field.name: Feedback.name.field.help_text,
            Feedback.text.field.name: Feedback.text.field.help_text,
            Feedback.mail.field.name: Feedback.mail.field.help_text,
        }

from django.contrib import admin

from feedback.models import Feedback, StatusLog

__all__ = []

admin.site.register(StatusLog)


@admin.register(Feedback)
class FeedbackAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        if change:
            feedback = Feedback.objects.get(pk=obj.pk)
            from_status = feedback.status
            to_status = obj.status

            if from_status != to_status:
                StatusLog.objects.create(
                    user=request.user,
                    feedback=feedback,
                    from_status=from_status,
                    to=to_status,
                )

        super().save_model(request, obj, form, change)

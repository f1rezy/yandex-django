from django.test import Client, TestCase
from django.urls import reverse
from parameterized import parameterized

from feedback.forms import FeedbackForm
from feedback.models import Feedback

__all__ = []


class FormTests(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.form = FeedbackForm()
        cls.form_data = {
            "name": "Test name",
            "text": "Test text",
            "mail": "test@gmail.com",
        }

    def test_feedback_page_show_correct_context(self):
        response = Client().get(
            reverse("feedback:feedback"),
        )
        self.assertIn("form", response.context)

    @parameterized.expand(
        [
            (Feedback.text.field.name, Feedback.text.field.verbose_name),
            (Feedback.mail.field.name, Feedback.mail.field.verbose_name),
        ],
    )
    def test_feedback_form_labels(self, field, expected_label):
        label = FormTests.form.fields[field].label
        self.assertEqual(label, expected_label)

    @parameterized.expand(
        [
            (Feedback.text.field.name, Feedback.text.field.help_text),
            (Feedback.mail.field.name, Feedback.mail.field.help_text),
        ],
    )
    def test_feedback_form_help_texts(self, field, expected_help_text):
        help_text = FormTests.form.fields[field].help_text
        self.assertEqual(help_text, expected_help_text)

    def test_feedback_form_redirect(self):
        response = Client().post(
            reverse("feedback:feedback"),
            data=FormTests.form_data,
            follow=True,
        )

        self.assertRedirects(response, reverse("feedback:feedback"))
        self.assertIn("Форма успешно отправлена!".encode(), response.content)

    def test_feedback_form_save_model(self):
        feedbacks_count = Feedback.objects.count()

        Client().post(
            reverse("feedback:feedback"),
            data=FormTests.form_data,
        )

        self.assertEqual(
            Feedback.objects.count(),
            feedbacks_count + 1,
        )

    @parameterized.expand(
        [
            (
                {
                    "text": "",
                    "mail": "",
                },
                {
                    "text": "Обязательное поле.",
                    "mail": "Обязательное поле.",
                },
            ),
            (
                {
                    "text": "",
                    "mail": "examplemail.ru",
                },
                {
                    "text": "Обязательное поле.",
                    "mail": "Введите правильный адрес электронной почты.",
                },
            ),
        ],
    )
    def test_feedback_form_negative_validator(self, form_data, field_errors):
        response = Client().post(
            reverse("feedback:feedback"),
            data=form_data,
        )
        for field in field_errors:
            self.assertFormError(response, "form", field, field_errors[field])

from django.conf import settings
from django.contrib import messages
from django.core.mail import send_mail
from django.urls import reverse_lazy
from django.views import generic

from feedback.forms import FeedbackForm
from feedback.models import Feedback

__all__ = []


class FeedbackView(generic.FormView):
    model = Feedback
    form_class = FeedbackForm
    template_name = "feedback/feedback.html"
    success_url = reverse_lazy("feedback:feedback")

    def form_valid(self, form):
        name = form.cleaned_data.get("name")
        text = form.cleaned_data.get("text")
        mail = form.cleaned_data.get("mail")
        send_mail(
            name,
            text,
            settings.MAIL,
            [mail],
            fail_silently=False,
        )
        form.save()
        messages.success(self.request, "Форма успешно отправлена!")
        return super().form_valid(form)

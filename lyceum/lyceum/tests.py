from django.test import Client, TestCase
from django.urls import reverse

__all__ = []


class ContextProcessorsTest(TestCase):
    def test_context_processor_show_correct_context(self):
        response = Client().get(
            reverse("homepage:home"),
        )
        self.assertIn("birthday_users", response.context)

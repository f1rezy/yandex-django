import re

from django.conf import settings

__all__ = []


WORDS_REGEX = re.compile(r"\w+|\W+")
RUSSIAN_REGEX = re.compile(r"\b([а-яА-ЯёЁ]+)\b")


class ReverseRussianMiddleware:
    cnt = 0

    def __init__(self, get_response):
        self.get_response = get_response

    @classmethod
    def check_need_reverse(cls):
        if not settings.REVERSE_RUSSIAN:
            return False

        cls.cnt += 1
        if cls.cnt != settings.REVERSE_RUSSIAN_COUNTER:
            return False
        cls.cnt = 0
        return True

    def __call__(self, request):
        if not self.check_need_reverse():
            return self.get_response(request)

        response = self.get_response(request)
        content = response.content.decode()
        words = WORDS_REGEX.findall(content)

        transformed = [
            word[::-1] if RUSSIAN_REGEX.search(word) else word
            for word in words
        ]

        response.content = "".join(transformed).encode()
        return response

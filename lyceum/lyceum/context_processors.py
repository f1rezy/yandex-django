import datetime

from django.utils import timezone

from users.models import User

__all__ = []


def birthdays(request):
    now = timezone.now()
    date_range = [
        now - datetime.timedelta(days=1),
        now + datetime.timedelta(days=1),
    ]
    return {
        "birthday_users": User.objects.active().filter(
            profile__birthday__month__range=[
                date_range[0].month,
                date_range[1].month,
            ],
            profile__birthday__day__range=[
                date_range[0].day,
                date_range[1].day,
            ],
        ),
    }

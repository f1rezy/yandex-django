from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
import django.contrib.auth.urls
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path

import about.urls
import catalog.urls
import download.urls
import feedback.urls
import homepage.urls
import rating_statistics.urls
import users.urls

urlpatterns = [
    path("", include(homepage.urls)),
    path("about/", include(about.urls)),
    path("catalog/", include(catalog.urls)),
    path("download/", include(download.urls)),
    path("feedback/", include(feedback.urls)),
    path("statistics/", include(rating_statistics.urls)),
    path("admin/", admin.site.urls),
    path("users/", include(users.urls)),
    path("auth/", include(django.contrib.auth.urls)),
    path("ckeditor5/", include("django_ckeditor_5.urls")),
]

if settings.DEBUG:
    if settings.MEDIA_ROOT:
        urlpatterns += static(
            settings.MEDIA_URL,
            document_root=settings.MEDIA_ROOT,
        )
    urlpatterns += staticfiles_urlpatterns()


if settings.DEBUG:
    import debug_toolbar

    urlpatterns += (path("__debug__/", include(debug_toolbar.urls)),)

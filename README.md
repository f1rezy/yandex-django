# yandex-django

[![pipeline-badge](https://gitlab.com/f1rezy/yandex-django/badges/main/pipeline.svg)](https://gitlab.com/f1rezy/yandex-django/commits/main)

## Создание и активация виртуального окружения

```bash
python3 -m venv venv
source venv/bin/activate
```

## Установка зависимостей

Основная:

```bash
pip install -r requirements/prod.txt
```

Для разработки:

```bash
pip install -r requirements/dev.txt
```

Для тестирования:

```bash
pip install -r requirements/test.txt
```

## Конфигурация .env

Скопируйте файл `.env.example` в `.env` и, если нужно, отредактируйте значения переменных

```bash
cp .env.example .env
```

## Создание базы данных

Чтобы создать базу данных, выполните миграции:

```bash
python3 lyceum/manage.py migrate
```

Для загрузки данных из фикстуры, выполните следующую команду:

```bash
python3 lyceum/manage.py loaddata lyceum/fixtures/data.json
```

## Запуск сервера

```bash
cd lyceum
django-admin compilemessages # Создание файлов локализации
python3 manage.py runserver
```

### ER-диаграмма базы данных

![er-diagram](ER.jpg)
